from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name
    
class Format(models.Model):
        name = models.CharField(max_length=200, db_index=True)
        slug = models.SlugField(max_length=200, db_index=True, unique=True)
        class Meta:
              ordering = ('name',)
              verbose_name = 'Форма'
              verbose_name_plural = 'Формы'

        def __str__(self):
            return self.name         

    
class glasses(models.Model):
    name = models.CharField(max_length=250)
    brand = models.CharField(max_length=250)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    format = models.ManyToManyField(Format)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Очки'
        verbose_name_plural = 'Очки'

    def __str__(self):
        return self.name



