

from django.contrib import admin

from zap.views import index

from django.urls import path
from optic.views import glassesListApiView
from optic.views import glassesRetriveApiView
from zap.views import zapListApiView
from zap.views import zapRetriveApiView


urlpatterns = [
        path('admin/', admin.site.urls),
        path('', index, name='index'),
        path('api/v1/list/',
              glassesListApiView.as_view(),
              name='list'),
        path('api/v1/list/<pk>/',
              glassesRetriveApiView.as_view(),
              name='retrive'),
        path('api/v1/zap/',
              zapListApiView.as_view(),
              name='list'), 
        path('api/v1/zap/<pk>/',
              zapRetriveApiView.as_view(),
              name='retrive'),           
]
