from django.core.management.base import BaseCommand
from oauth2client.service_account import ServiceAccountCredentials
import httplib2
import os.path
from googleapiclient.discovery import build
from zap.models import Product, Catalog, Brand


class Command(BaseCommand):
    sheet_id = "1N6zMK3uSowkxZEnlbOvm_ZeaHebtl7rgcdqBoXNV5Ik" # код документа

    def get_service_sacc(self):
        creds_json = os.path.dirname(__file__) + "/../../sheets.json" # название файла с токенами
        scopes = ['https://www.googleapis.com/auth/spreadsheets']

        creds_service = ServiceAccountCredentials.from_json_keyfile_name(creds_json, scopes).authorize(httplib2.Http())
        return build('sheets', 'v4', http=creds_service)
    #print(build)

    def handle(self, *args, **options):

        sheet = self.get_service_sacc().spreadsheets()
        response = sheet.values().get(spreadsheetId=self.sheet_id, range='A2:G50').execute() #название таблицы

        #catalog = ""

        for row in response.get("values", []):

            index = row[0]  # индекс вообще нахрен не нужная информация, так как в твоей таблице это просто порядковый номер, как я понял
            description = row[1]   # Название каталога, например Амортизатор
            catalog = row[2]
            # if not (catalog_title:=row[2]) == "":
            #  catalog = catalog_title
            article = row[3]    # артикул, он нужен
            price = row[4]  # цена, она тоже нужна
            amount = row[5]  # кол-во единиц на складе
            brand = row[6]  # брэнд производителя

            print(index, description, catalog, article, price, amount, brand)
            
        
            catalog_from_base = Catalog.objects.filter(name = catalog).first()
            if not catalog_from_base:
               catalog_from_base = Catalog.objects.create( 
                name = catalog
                )
            print(catalog_from_base)

            brand_from_base = Brand.objects.filter(name = brand).first()
            if not brand_from_base:
                 brand_from_base = Brand.objects.create(
                name = brand
                )
            print(brand_from_base)
            
            product_from_base = Product.objects.filter(article = article).first()
            if product_from_base: # если продукт по артикулу был найден, его нужно обновить теми данными, которые ты достал из таблицы
               product_from_base.description = description
               product_from_base.catalog = catalog_from_base # присваиваем каталог, который либо создали, либо нашли
               product_from_base.amount = amount # присваиваем свежее количество
               product_from_base.price = price # присваиваем свежую цену
               product_from_base.brand = brand_from_base # присваиваем продукту брэнд, который либо нашли, либо создали

               product_from_base.save() # сохраняем продукт с новыми данными
               print(f"Продукт в каталоге {product_from_base.article} с артикулом {article} был обновлен!")


            else: # else работает в паре с предыдущим if, в данном случае если продукт в базе найден не был, его надо создать
                 Product.objects.create(
                 description = description,     
                 catalog = catalog_from_base,
                 article = article,
                 amount = amount,
                 price = price,
                 brand = brand_from_base)
        
                 print(f"Продукт в каталоге {brand_from_base.name} с артикулом {article} был создан!")


