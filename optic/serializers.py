from rest_framework import serializers
from .models import glasses, Category, Format


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name','slug']

class FormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Format
        fields = ['name','slug']       

class glassesSerializer(serializers.ModelSerializer):
    cat = CategorySerializer(many=True, read_only=True)
    form = FormatSerializer(many=True, read_only=True)
    class Meta:
        model = glasses
        fields = ('id','name','brand','price','cat','form')

