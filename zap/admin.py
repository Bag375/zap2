from django.contrib import admin


from .models import Product, Brand, Catalog 


admin.site.register(Product)
admin.site.register(Brand)
admin.site.register(Catalog)
