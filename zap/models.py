from django.db import models


class Brand(models.Model):
      name = models.CharField(max_length=200, db_index=True)
      #slug = models.SlugField(max_length=200, db_index=True, unique=True)
      class Meta:
            ordering = ('name',)
            verbose_name = 'Фирма'
            verbose_name_plural = 'Фирмы'
      
      def __str__(self):
       return self.name  
      
class Catalog(models.Model):
      name = models.CharField(max_length=200, db_index=True)
      #slug = models.SlugField(max_length=200, db_index=True, unique=True)
      class Meta:
            ordering = ('name',)
            verbose_name = 'Каталог автозапчастей'
            verbose_name_plural = 'Каталоги автозапчастей'
      
      def __str__(self):
       return self.name     
      

class Product(models.Model):
    description = models.CharField(max_length=250)
    catalog = models.ForeignKey(Catalog, on_delete=models.PROTECT)
    article = models.CharField(max_length=250)
    amount = models.IntegerField()
    price = models.IntegerField()
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT)
    class Meta:
        ordering = ('description',)
        verbose_name = 'Автозапчасть'
        verbose_name_plural = 'Автозапчасти'

    def __str__(self):
        return self.description
    
