from rest_framework import serializers
from .models import Product, Catalog, Brand


class CatalogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Catalog
        fields = ['name']

class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['name']       

class zapSerializer(serializers.ModelSerializer):
    cat = CatalogSerializer(many=True, read_only=True)
    brand_mod = BrandSerializer(many=True, read_only=True)
    class Meta:
        model = Product
        fields = ('id', 'description', 'catalog', 'article','price','brand','cat','brand_mod')