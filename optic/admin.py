from django.contrib import admin
from .models import glasses, Category,Format


admin.site.register(glasses)
admin.site.register(Category)
admin.site.register(Format)