from django.shortcuts import render
#from django.http import HttpResponse, HttpResponseNotFound, Http404

from rest_framework import generics
from .models import Product
from .serializers import zapSerializer
#from .serializers import glasses_retriver_Serializer
#from rest_framework.generics import RetriveAPIView
#from courses.api.serializers import SubjectSerializer
 
class zapListApiView(generics.ListAPIView):
      queryset = Product.objects.all()
      serializer_class = zapSerializer

class zapRetriveApiView(generics.RetrieveAPIView):
      queryset = Product.objects.all()
      serializer_class = zapSerializer

def index(request):
      return render(request, 'zap/index.html', {'title': 'ЗАПЧАСТИ'})