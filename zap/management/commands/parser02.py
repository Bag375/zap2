from zap.models import Product
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand
import requests
import math

    
class Command(BaseCommand):
    help = "Parsing https://euroauto.ru/"

    def handle(self, *args, **options):
        START_PAGE = 'https://euroauto.ru/firms/'

        for product in Product.objects.all():
            brand = product.brand
            part_number = product.article

            url = f"{START_PAGE}{brand}/{part_number}/"
            soup = BeautifulSoup(requests.get(url).content, "html.parser")
            container = soup.find('div', {"class":"num-listing-group-wrap"})
            prices = container.find_all('div', {"class":"num-price"})
            if len(prices):
                prices_list = [price.text.strip().lower() for price in prices]
                true_prices = []
                if len(prices_list):
                    for price in prices_list:
                        if price.isnumeric():
                            true_prices.append(price)
                    print("Весь список цен", prices_list)
                    print("Список цен в формате числа", true_prices)
                    prices_sum = sum(true_prices)
                    average_price = math.ceil(prices_sum/len(true_prices))
                     
                    print("средняя цена по больнице", average_price)
                    product.price = average_price
                    product.save()
                else:
                    print("Нативных цен не существует, возможно цена приходит в формате 'От ...'")
            else:
                print("Цены не найдены, проверьте существует ли страница")