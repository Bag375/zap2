var viewModel = {
    firstName: ko.observable(''),
    lastName: ko.observable('')
    
};
viewModel.fullName = ko.computed(function() {
    return this.firstName() + ' ' + this.lastName();
}, viewModel);


ko.applyBindings(viewModel); 
