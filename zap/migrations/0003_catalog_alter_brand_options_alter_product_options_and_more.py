# Generated by Django 4.2.3 on 2023-07-27 00:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('zap', '0002_alter_brand_options'),
    ]

    operations = [
        migrations.CreateModel(
            name='Catalog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=200)),
                ('slug', models.SlugField(max_length=200, unique=True)),
            ],
            options={
                'verbose_name': 'Каталог автозапчастей',
                'verbose_name_plural': 'Каталоги автозапчастей',
                'ordering': ('name',),
            },
        ),
        migrations.AlterModelOptions(
            name='brand',
            options={'ordering': ('name',), 'verbose_name': 'Фирма', 'verbose_name_plural': 'Фирмы'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ('price',), 'verbose_name': 'Автозапчасть', 'verbose_name_plural': 'Автозапчасти'},
        ),
        migrations.RenameField(
            model_name='product',
            old_name='brand',
            new_name='Brand',
        ),
        migrations.RemoveField(
            model_name='product',
            name='name',
        ),
        migrations.AddField(
            model_name='product',
            name='Catalog',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.PROTECT, to='zap.catalog'),
            preserve_default=False,
        ),
    ]
