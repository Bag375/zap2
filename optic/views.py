#from django.shortcuts import render

from rest_framework import generics
from .models import glasses
from .serializers import glassesSerializer
#from .serializers import glasses_retriver_Serializer
#from rest_framework.generics import RetriveAPIView
#from courses.api.serializers import SubjectSerializer
 
class glassesListApiView(generics.ListAPIView):
      queryset = glasses.objects.all()
      serializer_class = glassesSerializer

class glassesRetriveApiView(generics.RetrieveAPIView):
      queryset = glasses.objects.all()
      serializer_class = glassesSerializer
      
